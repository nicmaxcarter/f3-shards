<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->set('DEBUG',1);
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('app/config.ini');

$f3->route('GET /',
	function($f3) {
		$f3->set('content','welcome.htm');
		echo Template::instance()->render('layout.html');
	}
);

$f3->route('GET /userref',
	function($f3) {
		$f3->set('content','userref.htm');
		echo Template::instance()->render('layout.html');
	}
);

$f3->run();
