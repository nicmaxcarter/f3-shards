<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shards Dashboard Lite - Free Bootstrap Admin Template –
      DesignRevision</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin
      dashboard template pack that comes with lots of templates and
      components.">
    <meta name="viewport" content="width=device-width, initial-scale=1,
      shrink-to-fit=no">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"
      rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
      crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0"
      href="assets/css/shards-dashboards.1.1.0.min.css">
    <link rel="stylesheet" href="assets/css/extras.1.1.0.min.css">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
  </head>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <?php echo $this->render('modules/sidebar.html',NULL,get_defined_vars(),0); ?>
          <!-- End Main Sidebar -->
          <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2
            offset-md-3">

            <?php echo $this->render('modules/navbar.html',NULL,get_defined_vars(),0); ?>

              <div class="main-content-container container-fluid px-4">
                <!-- Page Header -->
                <!-- <div class="page-header row no-gutters py-4">
                  <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                    <span class="text-uppercase page-subtitle">Dashboard</span>
                    <h3 class="page-title">Blog Overview</h3>
                  </div>
                </div> -->
                <!-- End Page Header -->
                
                <?php echo $this->render($content,NULL,get_defined_vars(),0); ?>
                <!---->
              </div>
              <!-- <footer class="main-footer d-flex p-2 px-3 bg-white border-top">
                <ul class="nav">
                  <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Products</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Blog</a>
                  </li>
                </ul>
                <span class="copyright ml-auto my-auto mr-2">Copyright © 2019
                  <a href="https://designrevision.com" rel="nofollow">DesignRevision</a>
                </span>
              </footer> -->
              
            </main>
          </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>
        <script
          src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
          integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
          crossorigin="anonymous"></script>
        <script
          src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
          integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
          crossorigin="anonymous"></script>
        <script
          src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
        <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
        <script
          src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
        <script src="assets/js/extras.1.1.0.min.js"></script>
        <script src="assets/js/shards-dashboards.1.1.0.min.js"></script>
        <script src="assets/js/app/app-blog-overview.1.1.0.js"></script>
      </body>
    </html>